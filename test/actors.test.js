const supertest = require('supertest');
const app = require('../app');

var key="";
var id="";
//sentencia
describe('Probar el sistema de autenticacion', ()=>{
  //casos de prueba => %50
  it('Deberia de obtener un login con usuario y contraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email':'correo@algo.com', 'password':'abcd1234'}) //login exitoso agregar usuario y pass
    .expect(200) // status HTTP code
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('probar las rutas de los actores', ()=>{

  it('deberia de obtener la lista de actores', (done)=>{
    supertest(app).get('/actors/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  })
  it('Deberia de crear un actor', (done)=>{
      supertest(app).post(`/actors/${id}`)
      .send({
          name: "prueba",
          lastName: "123"

      })
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
          if(err){
              done(err);
          }else{
              expect(res.statusCode).toEqual(200);
              done();
          }
      })
    })
    it('Deberia de encontrar un actor especifico', (done)=>{
        supertest(app).get(`/actors/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de editar un actor especifico', (done)=>{
        supertest(app).patch(`/actors/${id}`)
        .send({
            name: "prueba",
            lastName: "123"

        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de reemplazar un actor especifico', (done)=>{
        supertest(app).put(`/actors/${id}`)
        .send({
            name: "prueba",
            lastName: "123"

        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia eliminar un actor especifico', (done)=>{
        supertest(app).delete(`/actors/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
});

const supertest = require('supertest');
const app = require('../app');

var key="";
var id="";
//sentencia
describe('Probar el sistema de autenticacion', ()=>{
  //casos de prueba => %50
  it('Deberia de obtener un login con usuario y contraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email':'correo@algo.com', 'password':'abcd1234'}) //login exitoso agregar usuario y pass
    .expect(200) // status HTTP code
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('probar las rutas de los members', ()=>{
  it('deberia de obtener la lista de los members', (done)=>{
    supertest(app).get('/members/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  })
  it('Deberia de crear un member', (done)=>{
      supertest(app).post(`/members/${id}`)
      .send({
          name:"prueba",
          lastName:"123",
          phone: 6142222222,
          status: true,
          city:"CUU",
          country:"Mexico",
          number: 4235,
          state:"CUU",
          street: "Calle"

      })
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
          if(err){
              done(err);
          }else{
              expect(res.statusCode).toEqual(200);
              done();
          }
      })
  })
    it('Deberia de encontrar un member especifico', (done)=>{
        supertest(app).get(`/members/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de editar un member especifico', (done)=>{
        supertest(app).patch(`/members/${id}`)
        .send({
          name:"prueba",
          lastName:"123",
          phone: 6142222222,
          status: true,
          city:"CUU",
          country:"Mexico",
          number: 4235,
          state:"CUU",
          street: "Calle"

        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de reemplazar un member especifico', (done)=>{
        supertest(app).put(`/members/${id}`)
        .send({
          name:"prueba",
          lastName:"123",
          phone: 6142222222,
          status: true,
          city:"CUU",
          country:"Mexico",
          number: 4235,
          state:"CUU",
          street: "Calle"

        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia eliminar un member especifico', (done)=>{
        supertest(app).delete(`/members/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
});

const supertest = require('supertest');
const app = require('../app');

var key="";
var id="";
//sentencia
describe('Probar el sistema de autenticacion', ()=>{
  //casos de prueba => %50
  it('Deberia de obtener un login con usuario y contraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email':'correo@algo.com', 'password':'abcd1234'}) //login exitoso agregar usuario y pass
    .expect(200) // status HTTP code
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('probar las rutas de los bookings', ()=>{
  it('deberia de obtener la lista de los bookings', (done)=>{
    supertest(app).get('/bookings/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  })
  it('Deberia de crear un booking', (done)=>{
      supertest(app).post(`/bookings/${id}`)
      .send({
          copy: 1,
          member: 1,
          date: "2-15-2021"

      })
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
          if(err){
              done(err);
          }else{
              expect(res.statusCode).toEqual(200);
              done();
          }
      })
  })
    it('Deberia de encontrar un booking especifico', (done)=>{
        supertest(app).get(`/bookings/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de editar un booking especifico', (done)=>{
        supertest(app).patch(`/bookings/${id}`)
        .send({
            copy: 1,
            member: 1,
            date: "2-15-2021"

        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de reemplazar un booking especifico', (done)=>{
        supertest(app).put(`/bookings/${id}`)
        .send({
          copy: 1,
          member: 1,
          date: "2-15-2021"

        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia eliminar un booking especifico', (done)=>{
        supertest(app).delete(`/bookings/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
});

const supertest = require('supertest');
const app = require('../app');

var key="";
var id="";
//sentencia
describe('Probar el sistema de autenticacion', ()=>{
  //casos de prueba => %50
  it('Deberia de obtener un login con usuario y contraseña correcto', (done)=>{
    supertest(app).post('/login')
    .send({'email':'correo@algo.com', 'password':'abcd1234'}) //login exitoso agregar usuario y pass
    .expect(200) // status HTTP code
    .end(function(err, res){
      key = res.body.obj;
      done();
    });
  });
});

describe('probar las rutas de los movies', ()=>{
  it('deberia de obtener la lista de los movies', (done)=>{
    supertest(app).get('/movies/')
    .set('Authorization', `Bearer ${key}`)
    .end(function(err, res){
      if(err){
        done(err);
      }else{
        expect(res.statusCode).toEqual(200);
        done();
      }
    });
  })
  it('Deberia de crear un movie', (done)=>{
      supertest(app).post(`/movies/${id}`)
      .send({
          genre: "terror",
          title: "Este semestre",
          director:"",
          actors:"",

      })
      .set('Authorization', `Bearer ${key}`)
      .end(function(err, res){
          if(err){
              done(err);
          }else{
              expect(res.statusCode).toEqual(200);
              done();
          }
      })
  })
    it('Deberia de encontrar un movie especifico', (done)=>{
        supertest(app).get(`/movies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de editar un movie especifico', (done)=>{
        supertest(app).patch(`/movies/${id}`)
        .send({
          genre: "terror",
          title: "Este semestre",
          director:"",
          actors:"",

        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia de reemplazar un movie espcifico', (done)=>{
        supertest(app).put(`/movies/${id}`)
        .send({
          genre: "terror",
          title: "Este semestre",
          director:"",
          actors:"",
        })
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
    it('Deberia eliminar un movie especifico', (done)=>{
        supertest(app).delete(`/movies/${id}`)
        .set('Authorization', `Bearer ${key}`)
        .end(function(err, res){
            if(err){
                done(err);
            }else{
                expect(res.statusCode).toEqual(200);
                done();
            }
        })
    })
});

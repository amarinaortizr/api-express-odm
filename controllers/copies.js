const express = require('express');
const Copy = require('../models/copy');

function list(req, res, next) {
    Copy.find().populate('_movie').then(objs => res.status(200).json({
        message: res.__('ok.copies.list'),
        objs: objs
    })).catch(err => res.status(500).json({
        message:res.__('error.copies.list'),
        objs: err
    }));
}

function index(req, res, next) {
    const id = req.params.id;
    Copy.findOne({'_id':id}).populate('_movie').then(obj => res.status(200).json({
        message: res.__('ok.copies.index'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.copies.index'),
        objs: err
    }));
}

function create(req, res, next) {
    const {
        format,
        movie,
        number,
        status
    } = req.body;

    let copy = new Copy({
        format: format,
        movie: movie,
        number: number,
        status: status
    });

    copy.save().then(obj => res.status(200).json({
        message: res.__('ok.copies.create'),
        objs: obj
    })).catch(err => res.status(500).json({
        message:  res.__('error.copies.create'),
        objs: err
    }));
}

function edit(req, res, next) {
    const id = req.params.id;
    const copy = new Object();

    const {
        format,
        movie,
        number,
        status
    } = req.body;

    if(format){
        copy._format = format;
    }

    if(movie){
        copy._movie = movie;
    }

    if(number){
        copy._number = number;
    }

    if(status){
        copy._status = status;
    }


    Copy.findOneAndUpdate({"_id":id}, copy).then(
        obj=>res.status(200).json({
            message:  res.__('ok.copies.edit'),
            obj: obj
        })
    ).catch(
        ex=>res.status(500).json({
            message:  res.__('error.copies.edit'),
            obj: ex
        })
    );
}

function replace(req, res, next) {
    const id = req.params.id;
    const {
        format,
        movie,
        number,
        status
    } = req.body;

    let copy = new Object({
        _format: format,
        _movie: movie,
        _number: number,
        _status: status
    });

    Copy.findOneAndReplace({'_id':id}, copy).then(obj => res.status(200).json({
        message:  res.__('ok.copies.replace'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.copies.replace'),
        objs: err
    }));
}

function destroy(req, res, next) {
    const id = req.params.id;
    Copy.remove({'_id':id}).then(obj => res.status(200).json({
        message: res.__('ok.copies.destroy'),
        objs: obj
    })).catch(err => res.status(500).json({
        message: res.__('error.copies.destroy'),
        objs: err
    }));
}

module.exports = {
    create, list, index, edit, replace, destroy
}

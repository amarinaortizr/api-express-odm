const express = require('express');
const Actor = require('../models/actor');

//RESTFULL => GET, POST, PATCH, DELETE
//Modelo=(Una estructura de datos que representa una entidad del mundo real)
function list(req, res, next) {
  Actor.find().then(objs => res.status(200).json({
    message: res.__('ok.actors.list'),
    obj:objs
  })).catch(ex => res.status(500).json({
    message: res.__('error.actors.list'),
    obj: ex
  }));

}

function index(req, res, next){
  const id = req.params.id;
  Actor.findOne({"_id":id}).then(obj => res.status(200).json({
    message: res.__('ok.actors.index'),
    obj: obj
  })).catch(ex => res.status(500).json({
    message: res.__('error.actors.index'),
    obj: ex
  }));
}

function create(req, res, next){
  const name = req.body.name;
  const lastName = req.body.lastName;

  let actor = new Actor({
    name:name,
    lastName:lastName
  });

  actor.save().then(obj => res.status(200).json({
    message: res.__('ok.actors.create'),
    obj: obj
  })).catch(ex => res.status(500).json({
    message: res.__('error.actors.create'),
    obj: ex
  }));

}

function replace(req, res, next){
  const id = req.params.id;
  let name = req.body.name ? req.body.name: "";
  let lastName = req.body.lastName ? req.body.lastName: "";

  let actor = new Object({
    _name: name,
    _lastName: lastName
  });

  Actor.findOneAndUpdate({"_id":id}, actor).then(obj => res.status(200).json({
    message:res.__('ok.actors.replace'),
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__('error.actors.replace'),
    obj: ex
  }));

}

function edit(req, res, next){

  const id = req.params.id;
  let name = req.body.name;
  let lastName = req.body.lastName;

  let actor = new Object();

  if(name){
    actor._name = name;
  }

  if(lastName){
    actor._lastName = lastName;
  }

  Actor.findOneAndUpdate({"_id":id}, actor).then(obj => res.status(200).json({
    message:res.__('ok.actors.edit'),
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__('error.actors.edit'),
    obj: ex
  }));

}

function destroy(req, res, next){
  const id = req.params.id;
  Actor.remove({"_id":id}).then(obj => res.status(200).json({
    message:res.__('ok.actors.destroy'),
    obj:obj
  })).catch(ex => res.status(500).json({
    message: res.__('error.actors.destroy'),
    obj: ex
  }));
}

module.exports={
  list, index, create, replace, edit, destroy
}
